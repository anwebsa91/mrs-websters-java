// specify the package
package userinterface;

// system imports

import javafx.event.Event;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.Date;
import java.util.Properties;
import java.util.Vector;

// project imports
import impresario.IModel;
import model.Book;
import model.Patron;

/** The class containing the Deposit Amount View  for the ATM application */
//==============================================================
public class InsertNewPatron extends View
{

    // Model

    // GUI components
    private TextField nameTF;
    private TextField addressTF;
    private TextField cityTF;
    private TextField stateTF;
    private TextField zipTF;
    private TextField emailTF;
    private TextField dobTF;
    private ComboBox status;

    private Button submitButton;
    private Button cancelButton;


    // For showing error message
    private MessageView statusLog;

    // constructor for this class -- takes a model object
    //----------------------------------------------------------
    public InsertNewPatron(IModel insertPatron)
    {
        super(insertPatron, "InsertNewPatron");

        // create a container for showing the contents
        VBox container = new VBox(10);
        container.setPadding(new Insets(15, 5, 5, 5));

        // create our GUI components, add them to this panel
        container.getChildren().add(createTitle());
        container.getChildren().add(createFormContent());

        // Error message area
        container.getChildren().add(createStatusLog("                          "));

        getChildren().add(container);

        populateFields();
    }


    // Create the label (Text) for the title
    //-------------------------------------------------------------
    private Node createTitle()
    {

        Text titleText = new Text("       INSERT NEW PATRON          ");
        titleText.setWrappingWidth(300);
        titleText.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        titleText.setTextAlignment(TextAlignment.CENTER);
        titleText.setFill(Color.BLACK);

        return titleText;
    }

    // Create the main form content
    //-------------------------------------------------------------
    private VBox createFormContent()
    {
        VBox vbox = new VBox(10);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));



        //TextFields
        nameTF = new TextField();

        addressTF = new TextField();

        cityTF = new TextField();

        stateTF = new TextField();

        zipTF = new TextField();

        emailTF = new TextField();

        dobTF = new TextField();


        //Labels
        Label nameLabel = new Label("Name:");
        Label addressLabel = new Label("Address:");
        Label cityLabel = new Label ("City:");
        Label stateLabel = new Label("State");
        Label zipLabel = new Label("Zip Code:");
        Label emailLabel = new Label("E-mail:");
        Label dobLabel = new Label("Date of Birth YYYY-MM-DD:");
        Label statusLabel = new Label("status:");

        //status combo box
        status = new ComboBox();
        status.getItems().addAll("Active", "Inactive");

        grid.add(nameLabel, 0, 0);
        grid.add(nameTF, 1, 0);
        grid.add(addressLabel, 0, 1);
        grid.add(addressTF, 1, 1);
        grid.add(cityLabel, 0, 2);
        grid.add(cityTF, 1, 2);
        grid.add(stateLabel, 0, 3);
        grid.add(stateTF, 1, 3);
        grid.add(zipLabel, 0, 4);
        grid.add(zipTF, 1, 4);
        grid.add(emailLabel, 0, 5);
        grid.add(emailTF, 1, 5);
        grid.add(dobLabel, 0, 6);
        grid.add(dobTF, 1, 6);

        grid.add(statusLabel, 0, 7);
        grid.add(status, 1, 7);

        submitButton = new Button("Submit");
        submitButton.setOnAction(e -> processAction(e));

        cancelButton = new Button("Back");
        cancelButton.setOnAction(e -> myModel.stateChangeRequest("LibrarianView", null));

        HBox btnContainer = new HBox(100);
        btnContainer.setAlignment(Pos.CENTER);
        btnContainer.getChildren().add(submitButton);
        btnContainer.getChildren().add(cancelButton);

        vbox.getChildren().add(grid);
        vbox.getChildren().add(btnContainer);

        return vbox;
    }

    // Create the status log field
    //-------------------------------------------------------------
    private MessageView createStatusLog(String initialMessage)
    {
        statusLog = new MessageView(initialMessage);

        return statusLog;
    }

    //-------------------------------------------------------------
    public void populateFields()
    {

        nameTF.setText("");
        addressTF.setText("");
        cityTF.setText("");
        stateTF.setText("");
        zipTF.setText("");
        emailTF.setText("");
        //dobTF.setText("");

    }

    // process events generated from our GUI components
    //-------------------------------------------------------------
    public void processAction(Event evt)
    {
        // DEBUG: System.out.println("DepositAmountView.processAction()");
        Patron addPatron = new Patron();
        clearErrorMessage();
        //Get the info from the text boxes
        String nameEntered = nameTF.getText();
        String addressEntered = addressTF.getText();
        String cityEntered = cityTF.getText();
        String stateEntered = stateTF.getText();
        String zipEntered = zipTF.getText();
        String emailEntered = emailTF.getText();
        String dobEntered = dobTF.getText();
        String statusEntered = status.getValue().toString();
        //Make properties objects from these values


            Properties patron = new Properties();
            patron.setProperty("name", nameEntered);
            patron.setProperty("address", addressEntered);
            patron.setProperty("city", cityEntered);
            patron.setProperty("stateCode", stateEntered);
            patron.setProperty("zip", zipEntered);
            patron.setProperty("email", emailEntered);
            patron.setProperty("dateOfBirth", dobEntered);
            patron.setProperty("status", statusEntered);

            addPatron.ProcessPatron(patron);
            addPatron.save();
            myModel.stateChangeRequest("LibrarianView", null);

    }

    /**
     * Process amount entered by user.
     * Action is to pass this info on to the transaction object.
     */
    //----------------------------------------------------------
    /*private void processAmount(String amount)
    {
        Properties props = new Properties();
        props.setProperty("Amount", amount);
        myModel.stateChangeRequest("Amount", props);
    }*/



    /**
     * Required by interface, but has no role here
     */
    //---------------------------------------------------------
    public void updateState(String key, Object value)
    {

    }

    /**
     * Display error message
     */
    //----------------------------------------------------------
    public void displayErrorMessage(String message)
    {
        statusLog.displayErrorMessage(message);
    }

    /**
     * Clear error message
     */
    //----------------------------------------------------------
    public void clearErrorMessage()
    {
        statusLog.clearErrorMessage();
    }

}
