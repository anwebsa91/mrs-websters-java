// specify the package
package userinterface;

// system imports

import javafx.event.Event;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.Date;
import java.util.Properties;
import java.util.Vector;

// project imports
import impresario.IModel;
import model.Book;
import model.BookCollection;
import model.Librarian;


/** The class containing the Deposit Amount View  for the ATM application */
//==============================================================
public class BookSearchView extends View
{

    // Model

    // GUI components
    private TextField searchTF;


    private Button submitButton;
    private Button cancelButton;


    // For showing error message
    private MessageView statusLog;

    // constructor for this class -- takes a model object
    //----------------------------------------------------------
    public BookSearchView(IModel bookSearch)
    {
        super(bookSearch, "BookSearchView");

        // create a container for showing the contents
        VBox container = new VBox(10);
        container.setPadding(new Insets(15, 5, 5, 5));

        // create our GUI components, add them to this panel
        container.getChildren().add(createTitle());
        container.getChildren().add(createFormContent());

        // Error message area
        container.getChildren().add(createStatusLog("                          "));

        getChildren().add(container);

        populateFields();
    }


    // Create the label (Text) for the title
    //-------------------------------------------------------------
    private Node createTitle()
    {

        Text titleText = new Text("       SEARCH FOR A BOOK          ");
        titleText.setWrappingWidth(300);
        titleText.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        titleText.setTextAlignment(TextAlignment.CENTER);
        titleText.setFill(Color.BLACK);

        return titleText;
    }

    // Create the main form content
    //-------------------------------------------------------------
    private VBox createFormContent()
    {
        VBox vbox = new VBox(10);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));



        //TextFields
        searchTF = new TextField();
        //Labels
        Label searchLabel = new Label("Enter Book Title: ");

        //status combo box

        grid.add(searchLabel, 0, 0);
        grid.add(searchTF, 0, 1);




        submitButton = new Button("Submit");
        submitButton.setOnAction(e -> processAction(e));

        cancelButton = new Button("Back");
        cancelButton.setOnAction(e -> myModel.stateChangeRequest("LibrarianView", null));

        HBox btnContainer = new HBox(100);
        btnContainer.setAlignment(Pos.CENTER);
        btnContainer.getChildren().add(submitButton);
        btnContainer.getChildren().add(cancelButton);

        vbox.getChildren().add(grid);
        vbox.getChildren().add(btnContainer);

        return vbox;
    }

    // Create the status log field
    //-------------------------------------------------------------
    private MessageView createStatusLog(String initialMessage)
    {
        statusLog = new MessageView(initialMessage);

        return statusLog;
    }

    //-------------------------------------------------------------
    public void populateFields()
    {

        searchTF.setText("");
    }

    // process events generated from our GUI components
    //-------------------------------------------------------------



    public void processAction(Event evt) {
        // DEBUG: System.out.println("DepositAmountView.processAction()");
        Properties book = new Properties();
        clearErrorMessage();
        //Get the info from the text boxes
        String enteredTitle = searchTF.getText();
        if(searchTF.getText() == null){
            displayErrorMessage("Enter a book title");
        }
        else {
            Librarian lib = new Librarian();
           // lib.searchBook(enteredTitle);
            BookCollection collection = lib.searchBook(enteredTitle);
          //  myModel.stateChangeRequest("bookList", collection);


            myModel.stateChangeRequest("SearchBooks", collection);
        }


    }

       /* String amountEntered = amount.getText();

        if ((amountEntered == null) || (amountEntered.length() == 0))
        {
            displayErrorMessage("Please enter an amount to deposit");
        }
        else
        {
            try
            {
                double amountVal = Double.parseDouble(amountEntered);
                if (amountVal <= 0)
                {
                    displayErrorMessage("Invalid amount: Please re-enter");
                }
                else
                {
                    processAmount(amountEntered);
                }
            }
            catch (Exception ex)
            {
                displayErrorMessage("Invalid amount: Please re-enter");
            }

        }*/


    /**
     * Process amount entered by user.
     * Action is to pass this info on to the transaction object.
     */
    //----------------------------------------------------------
    /*private void processAmount(String amount)
    {
        Properties props = new Properties();
        props.setProperty("Amount", amount);
        myModel.stateChangeRequest("Amount", props);
    }*/



    /**
     * Required by interface, but has no role here
     */
    //---------------------------------------------------------
    public void updateState(String key, Object value)
    {

    }

    /**
     * Display error message
     */
    //----------------------------------------------------------
    public void displayErrorMessage(String message)
    {
        statusLog.displayErrorMessage(message);
    }

    /**
     * Clear error message
     */
    //----------------------------------------------------------
    public void clearErrorMessage()
    {
        statusLog.clearErrorMessage();
    }

}
