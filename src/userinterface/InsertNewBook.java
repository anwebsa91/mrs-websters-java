// specify the package
package userinterface;

// system imports

import javafx.event.Event;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.Date;
import java.util.Properties;
import java.util.Vector;

// project imports
import impresario.IModel;
import model.Book;

/** The class containing the Deposit Amount View  for the ATM application */
//==============================================================
public class InsertNewBook extends View
{

    // Model

    // GUI components
    private TextField bookTitleTF;
    private TextField authorTF;
    private TextField pubYearTF;
    private ComboBox status;

    private Button submitButton;
    private Button cancelButton;


    // For showing error message
    private MessageView statusLog;

    // constructor for this class -- takes a model object
    //----------------------------------------------------------
    public InsertNewBook(IModel insertBook)
    {
        super(insertBook, "InsertNewBook");

        // create a container for showing the contents
        VBox container = new VBox(10);
        container.setPadding(new Insets(15, 5, 5, 5));

        // create our GUI components, add them to this panel
        container.getChildren().add(createTitle());
        container.getChildren().add(createFormContent());

        // Error message area
        container.getChildren().add(createStatusLog("                          "));

        getChildren().add(container);

        populateFields();
    }


    // Create the label (Text) for the title
    //-------------------------------------------------------------
    private Node createTitle()
    {

        Text titleText = new Text("       INSERT NEW BOOK          ");
        titleText.setWrappingWidth(300);
        titleText.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        titleText.setTextAlignment(TextAlignment.CENTER);
        titleText.setFill(Color.BLACK);

        return titleText;
    }

    // Create the main form content
    //-------------------------------------------------------------
    private VBox createFormContent()
    {
        VBox vbox = new VBox(10);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));



        //TextFields
        bookTitleTF = new TextField();
        bookTitleTF.editableProperty();
        authorTF = new TextField();
        authorTF.editableProperty();
        pubYearTF = new TextField();
        pubYearTF.editableProperty();
        //Labels
        Label bookTitleLabel = new Label("Book Title:");
        Label authorLabel = new Label("Author:");
        Label pubYearLabel = new Label ("Publication Year:");
        Label statusLabel = new Label("Status:");
        //status combo box
        status = new ComboBox();
        status.getItems().addAll("Active", "Inactive");

        grid.add(bookTitleLabel, 0, 0);
        grid.add(bookTitleTF, 1, 0);
        grid.add(authorLabel, 0, 1);
        grid.add(authorTF, 1, 1);
        grid.add(pubYearLabel, 0, 2);
        grid.add(pubYearTF, 1, 2);

        grid.add(statusLabel, 0, 3);
        grid.add(status, 1, 3);

        submitButton = new Button("Submit");
        submitButton.setOnAction(e -> processAction(e));

        cancelButton = new Button("Back");
        cancelButton.setOnAction(e -> myModel.stateChangeRequest("LibrarianView", null));

        HBox btnContainer = new HBox(100);
        btnContainer.setAlignment(Pos.CENTER);
        btnContainer.getChildren().add(submitButton);
        btnContainer.getChildren().add(cancelButton);

        vbox.getChildren().add(grid);
        vbox.getChildren().add(btnContainer);

        return vbox;
    }

    // Create the status log field
    //-------------------------------------------------------------
    private MessageView createStatusLog(String initialMessage)
    {
        statusLog = new MessageView(initialMessage);

        return statusLog;
    }

    //-------------------------------------------------------------
    public void populateFields()
    {

        bookTitleTF.setText("");
        authorTF.setText("");
        pubYearTF.setText("");
    }

    // process events generated from our GUI components
    //-------------------------------------------------------------
    public void processAction(Event evt)
    {
        // DEBUG: System.out.println("DepositAmountView.processAction()");

        clearErrorMessage();
        Properties book = new Properties();
        //Get the info from the text boxes
        String titleEntered = bookTitleTF.getText();
        String authorEntered = authorTF.getText();
        String pubYearEntered = pubYearTF.getText();
        String statusEntered = status.getValue().toString();
        //Make properties objects from these values
        int year = Integer.parseInt(pubYearEntered);
        Date date = new Date();
        int today = date.getYear();
        if(year < 1800 && year > today){
            displayErrorMessage("Enter a valid year");

        }
        else {
            if (titleEntered != null && authorEntered != null && pubYearEntered != null) {
                book.setProperty("bookTitle", titleEntered);
                book.setProperty("author", authorEntered);
                book.setProperty("pubYear", pubYearEntered);
            }
            if (statusEntered == null) {
                book.setProperty("status", "active");
            } else if (statusEntered != null) {
                book.setProperty("status", statusEntered);
            } else {
                displayErrorMessage("Enter required fileds");
            }
        }




        Book addBook = new Book();
        addBook.ProcessBook(book);


        myModel.stateChangeRequest("LibrarianView", null);
    }

    /**
     * Process amount entered by user.
     * Action is to pass this info on to the transaction object.
     */
    //----------------------------------------------------------
    /*private void processAmount(String amount)
    {
        Properties props = new Properties();
        props.setProperty("Amount", amount);
        myModel.stateChangeRequest("Amount", props);
    }*/



    /**
     * Required by interface, but has no role here
     */
    //---------------------------------------------------------
    public void updateState(String key, Object value)
    {

    }

    /**
     * Display error message
     */
    //----------------------------------------------------------
    public void displayErrorMessage(String message)
    {
        statusLog.displayErrorMessage(message);
    }

    /**
     * Clear error message
     */
    //----------------------------------------------------------
    public void clearErrorMessage()
    {
        statusLog.clearErrorMessage();
    }

}
