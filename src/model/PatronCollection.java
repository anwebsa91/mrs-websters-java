package model;
import java.util.Vector;
import event.Event;
import exception.InvalidPrimaryKeyException;
import impresario.IView;
import javafx.scene.Scene;
import userinterface.View;
import userinterface.ViewFactory;

import java.util.Properties;
import java.util.Vector;



public class PatronCollection extends model.EntityBase {
    private static final String myTableName = "Patron";
    private Vector<Patron> patrons;

    //Constructor that creates an empty patrons vector
    public PatronCollection() {
        super(myTableName);
        patrons = new Vector();
    }
    //-----------------------------------------------------------------------------


    private Vector<Patron> patronQueryHelper(String query)
    {
        Vector allDataRetrieved = getSelectQueryResult(query);

        if (allDataRetrieved != null) {
            //initilizing account vector, get a new set of account objects to populate
            patrons = new Vector<Patron>();
            //populates the patron vector
            for (int count = 0; count < allDataRetrieved.size(); count++) {
                Properties nextPatronData = (Properties) allDataRetrieved.elementAt(count);

                Patron patron = new Patron(nextPatronData);
                //Adding each account to a collection(accounts vector)
                //Have a seperate method to add account because the method will build up a sorted account collection
                if (patron != null) {
                    addPatron(patron);
                }

            }
        }
        return patrons;
    }

    //-----------------------------------------------------------------------------
    //Finds patrons older than dateOfBirth given by user
    public Vector<Patron> findPatronsOlderThan(String dateOfBirth)
    {
        Vector<Patron> oldPat = new Vector<>();
        String query = "SELECT * FROM " + myTableName + " WHERE dateOfBirth < '"+  dateOfBirth + "'";
        oldPat = patronQueryHelper(query);
        return oldPat;
    }

    //-----------------------------------------------------------------------------
    //Finds patrons younger than dateOfBirth given by user
    public Vector<Patron> findPatronsYoungerThan(String dateOfBirth)
    {
        Vector<Patron> youPat = new Vector<>();
        String query = "SELECT * FROM " + myTableName + " WHERE dateOfBirth > '"+  dateOfBirth + "'";
        youPat = patronQueryHelper(query);
        return youPat;
    }

    //-----------------------------------------------------------------------------
    //Finds patrons from zip given by user
    public Vector<Patron> findPatronsAtZipCode(String zip)
    {
        Vector<Patron> patZip = new Vector<Patron>();
        String query = "SELECT * FROM " + myTableName + " WHERE zip = "+  zip;
        patZip = patronQueryHelper(query);
        return patZip;
    }

    //-----------------------------------------------------------------------------
    //Finds patrons that match a pattern given by user
    public Vector<Patron> findPatronsWithNamesLike(String name)
    {
        Vector<Patron> namePat = new Vector<>();
        String query = "SELECT * FROM " + myTableName + " WHERE name like '%"+ name + "%'" ;
        namePat = patronQueryHelper(query);
        return namePat;
    }

    //----------------------------------------------------------------------------------
    //Adds a patron
    private void addPatron(Patron a) {
        //accounts.add(a);
        int index = findIndexToAdd(a);
        patrons.insertElementAt(a, index); // To build up a collection sorted on some key
    }


    //----------------------------------------------------------------------------------
    private int findIndexToAdd(Patron a) {
        //users.add(u);
        int low = 0;
        int high = patrons.size() - 1;
        int middle;

        while (low <= high) {
            middle = (low + high) / 2;

            Patron midSession = patrons.elementAt(middle);
            //compares the accounts to help with the sort binary search
            int result = Patron.compare(a, midSession);

            if (result == 0) {
                return middle;
            } else if (result < 0) {
                high = middle - 1;
            } else {
                low = middle + 1;
            }


        }
        return low;
    }
    //-----------------------------------------------------------------------------
    public Patron retrieve(String patronId) {
        Patron retValue = null;
        for (int cnt = 0; cnt < patrons.size(); cnt++) {
            Patron nextAcct = patrons.elementAt(cnt);
            String nextAccNum = (String) nextAcct.getState("patronId");
            if (nextAccNum.equals(patronId) == true) {
                retValue = nextAcct;
                return retValue; // we should say 'break;' here
            }
        }

        return retValue;
    }

    //----------------------------------------------------------
    public Object getState(String key) {
        if (key.equals("Patron"))
            return patrons;
        else if (key.equals("patrons"))
            return this;
        return null;
    }

    //----------------------------------------------------------------
    public void stateChangeRequest(String key, Object value) {

        myRegistry.updateSubscribers(key, this);
    }


    //----------------------------------------------------------

    public void updateState(String key, Object value) {
        stateChangeRequest(key, value);
    }

    //-----------------------------------------------------------------------------------
    //Every class must have this it has a schema to do the updates
    protected void initializeSchema(String tableName) {
        if (mySchema == null) {
            mySchema = getSchemaInfo(tableName);
        }


    }


}