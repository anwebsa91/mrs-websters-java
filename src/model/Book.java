package model;

import exception.InvalidPrimaryKeyException;
import model.Account;
import model.EntityBase;

import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

public class Book extends EntityBase {

    private static final String myTableName = "Book";

    // GUI Components

    private String updateStatusMessage = "";

    protected Properties dependencies;
    //This is to look up something that is already in the database
    public Book(){
        super(myTableName);
        setDependencies();
        persistentState = new Properties();
    }
    public Book(String bookId)
            throws InvalidPrimaryKeyException {
        //calling the constructor to the EntityBase by passing in the name of the table
        super(myTableName);
        setDependencies();
        //SQL Query to get all books with bookId's
        String query = "SELECT * FROM " + myTableName + " WHERE (bookId = " + bookId + ")";

        //creates a vector of all objects where the information from the table is related to the primary key
        Vector<Properties> allDataRetrieved = getSelectQueryResult(query);

        // You must get one account at least
        if (allDataRetrieved != null) {
            int size = allDataRetrieved.size();

            // There should be EXACTLY one account. More than that is an error
            if (size != 1) {
                throw new InvalidPrimaryKeyException("Multiple books matching id : "
                        + bookId + " found.");
            } else {
                // copy all the retrieved data into persistent state
                Properties retrievedAccountData = allDataRetrieved.elementAt(0);
                persistentState = new Properties();
                //This will get the name of our column tables
                Enumeration allKeys = retrievedAccountData.propertyNames();
                while (allKeys.hasMoreElements() == true) {
                    //nextKey is the column name, nextValue is the data associated with key
                    String nextKey = (String) allKeys.nextElement();
                    String nextValue = retrievedAccountData.getProperty(nextKey);
                    // bookId = Integer.parseInt(retrievedAccountData.getProperty("bookId"));

                    if (nextValue != null) {
                        persistentState.setProperty(nextKey, nextValue);

                    }
                }

            }
        }
        // If no account found for this user name, throw an exception
        else {
            throw new InvalidPrimaryKeyException("No book matching id : "
                    + bookId + " found.");
        }

    }
    //This is to add something into the database that doesn't exist
    public Book(Properties props) {
        super(myTableName);

        setDependencies();
        persistentState = new Properties();
        if (props != null) {
            Enumeration allKeys = props.propertyNames();
            while (allKeys.hasMoreElements() == true) {
                String nextKey = (String) allKeys.nextElement();
                String nextValue = props.getProperty(nextKey);

                if (nextValue != null) {
                    persistentState.setProperty(nextKey, nextValue);

                }
            }
        }
    }

    //-----------------------------------------------------------------------------------
    private void setDependencies() {

        dependencies = new Properties();

        myRegistry.setDependencies(dependencies);
    }
    //-----------------------------------------------------------------------------------
    public Object getState(String key) {

        if (key.equals("bookId") == true)
            return persistentState.getProperty("bookId");
        if (key.equals("author") == true)
            return persistentState.getProperty("author");
        if (key.equals("UpdateStatusMessage") == true)
            return updateStatusMessage;

        return persistentState.getProperty(key);
    }

    public void stateChangeRequest(String key, Object value) {
        myRegistry.updateSubscribers(key, this);


    }
    //-----------------------------------------------------------------------------------

    protected void initializeSchema(String tableName) {
        if (mySchema == null) {
            mySchema = getSchemaInfo(tableName);
        }
    }

    //-----------------------------------------------------------------------------------
    //Save the book into the database
    public void save() {

        updateStateInDatabase();
    }

    //-----------------------------------------------------------------------------------
    private void updateStateInDatabase() {
        try {
            if (persistentState.getProperty("bookId") != null) {
                Properties whereClause = new Properties();
                whereClause.setProperty("bookId",
                        persistentState.getProperty("bookId"));
                updatePersistentState(mySchema, persistentState, whereClause);
                updateStatusMessage = "Book information for BookID: " + persistentState.getProperty("bookId") + " updated successfully in database!";
            } else {
                Integer bookId =
                        insertAutoIncrementalPersistentState(mySchema, persistentState);
                persistentState.setProperty("bookId", "" + bookId.intValue());
                updateStatusMessage = "Book data for new Book: " + persistentState.getProperty("bookId")
                        + " added successfully to database!";
            }
        } catch (SQLException ex) {
            updateStatusMessage = "Error in adding book data to database!";
        }
        System.out.println(updateStatusMessage + "\n");
    }
    //-----------------------------------------------------------------------------------
    //Compare two books in the database by bookId
    public static int compare(Book a, Book b) {
        String aNum = (String) a.getState("bookId");
        String bNum = (String) b.getState("bookId");

        return aNum.compareTo(bNum);
    }
    //-----------------------------------------------------------------------------------
    public Vector<String> getEntryListView() {
        Vector<String> v = new Vector<String>();

        v.addElement(persistentState.getProperty("bookId"));
        v.addElement(persistentState.getProperty("bookTitle"));
        v.addElement(persistentState.getProperty("author"));
        v.addElement(persistentState.getProperty("pubYear"));
        v.addElement(persistentState.getProperty("status"));


        return v;
    }
    //-----------------------------------------------------------------------------------
    public String toString(){
        return("Book ID: " +   persistentState.getProperty("bookId")+
                " Book Title: " + persistentState.getProperty("bookTitle")
                + " Author: " + persistentState.getProperty("author")
                + " Publication Year: " + persistentState.getProperty("pubYear")
                + " Status: " + persistentState.getProperty("status") + '\n');
    }
    public void ProcessBook(Properties p){

        persistentState.setProperty("bookTitle", p.getProperty("bookTitle"));
        persistentState.setProperty("author", p.getProperty("author"));
        persistentState.setProperty("pubYear", p.getProperty("pubYear"));
        persistentState.setProperty("status", p.getProperty("status"));
        save();


    }
}