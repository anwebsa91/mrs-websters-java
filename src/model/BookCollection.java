package model;
import event.Event;
import exception.InvalidPrimaryKeyException;
import impresario.IView;
import javafx.scene.Scene;
import userinterface.View;
import userinterface.ViewFactory;

import java.util.Properties;
import java.util.Vector;

public class BookCollection extends model.EntityBase {

    private static final String myTableName = "Book";
    private Vector<Book> books;

    //Constructor for this class adding a book with bookid
    public BookCollection()
    {
        super(myTableName);
        books = new Vector();
    }
    //-----------------------------------------------------------------------------------
    //Constructor
    private Vector<Book> bookHelper(String query){
        Vector allDataRetrieved = getSelectQueryResult(query);

        if (allDataRetrieved != null) {
            //initilizing account vector, get a new set of account objects to populate
            books = new Vector<Book>();
            //populates the books vector
            for (int count = 0; count < allDataRetrieved.size(); count++) {
                Properties nextBookData = (Properties) allDataRetrieved.elementAt(count);

                Book book = new Book(nextBookData);
                //Adding each account to a collection(accounts vector)
                //Have a seperate method to add account because the method will build up a sorted account collection
                if (book != null) {
                    addBook(book);
                }


            }
        }
        return books;
    }
    //-----------------------------------------------------------------------------------
    //Finds all books older than a date given by user
    public Vector<Book> findBooksOlderThanDate(String year) {
        Vector<Book> booksC = new Vector<Book>();
        String query = "SELECT * FROM " + myTableName + " WHERE pubYear < " + year;
        booksC = bookHelper(query);
        return booksC;
    }
    //-----------------------------------------------------------------------------------
    //Finds all books newer than a date given by user
    public Vector<Book> findBooksNewerThanDate(String year) {
        Vector<Book> booksC = new Vector<Book>();
        String query = "SELECT * FROM " + myTableName + " WHERE pubYear > " + year;
        booksC = bookHelper(query);
        return booksC;
    }
    //-----------------------------------------------------------------------------------
    //Finds all books with the title given by user
    public Vector<Book> findBooksWithTitleLike(String title)
    {
        Vector<Book> booksC = new Vector<>();
        String query = "SELECT * FROM " + myTableName + " WHERE bookTitle LIKE '%" + title + "%'";
        booksC = bookHelper(query);
        return booksC;
    }
    //-----------------------------------------------------------------------------------
    //Finds all books with the author given by user
    public Vector<Book> findBooksWithAuthorLike(String author)
    {
        Vector<Book> booksC = new Vector<>();
        String query = "SELECT * FROM " + myTableName + " WHERE authorLIKE '%" + author + "%'";
        booksC =bookHelper(query);
        return booksC;
    }
    //-----------------------------------------------------------------------------------
    private void addBook(Book a) {
        //accounts.add(a);
        int index = findIndexToAdd(a);
        books.insertElementAt(a, index); // To build up a collection sorted on some key
    }
    //----------------------------------------------------------------------------------
    private int findIndexToAdd(Book a) {
        //users.add(u);
        int low = 0;
        int high = books.size() - 1;
        int middle;

        while (low <= high) {
            middle = (low + high) / 2;

            Book midSession = books.elementAt(middle);
            //compares the accounts to help with the sort binary search
            int result = Book.compare(a, midSession);

            if (result == 0) {
                return middle;
            } else if (result < 0) {
                high = middle - 1;
            } else {
                low = middle + 1;
            }
        }
        return low;
    }
    //----------------------------------------------------------
    public Object getState(String key) {
        if (key.equals("BookList"))
            return books;
        else if (key.equals("Books"))
            return this;
        return null;
    }

    //----------------------------------------------------------------
    public void stateChangeRequest(String key, Object value) {

        myRegistry.updateSubscribers(key, this);
    }
    //-----------------------------------------------------------------------------------
    public Book retrieve(String bookId) {
        Book retValue = null;
        for (int cnt = 0; cnt < books.size(); cnt++) {
            Book nextAcct = books.elementAt(cnt);
            String nextAccNum = (String) nextAcct.getState("bookId");
            if (nextAccNum.equals(bookId) == true) {
                retValue = nextAcct;
                return retValue; // we should say 'break;' here
            }
        }
        return retValue;
    }
    //----------------------------------------------------------

    public void updateState(String key, Object value) {
        stateChangeRequest(key, value);
    }
    //-----------------------------------------------------------------------------------
    //Every class must have this it has a schema to do the updates
    protected void initializeSchema(String tableName) {
        if (mySchema == null) {
            mySchema = getSchemaInfo(tableName);
        }
    }
}