package model;

import exception.InvalidPrimaryKeyException;

import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;


public class Patron extends EntityBase {

    private static final String myTableName = "Patron";

    // GUI Components

    private String updateStatusMessage = "";

    protected Properties dependencies;

    public Patron(String patronId)
            throws InvalidPrimaryKeyException {
        //calling the constructor to the EntityBase by passing in the name of the table
        super(myTableName);
        setDependencies();
        String query = "SELECT * FROM " + myTableName + " WHERE (patronId = " + patronId + ")";

        //creates a vector of all objects where the information from the table is related to the primary key
        Vector<Properties> allDataRetrieved = getSelectQueryResult(query);

        // You must get one account at least
        if (allDataRetrieved != null) {
            int size = allDataRetrieved.size();

            // There should be EXACTLY one account. More than that is an error
            if (size != 1) {
                throw new InvalidPrimaryKeyException("Multiple Patron accounts matching id : "
                        + patronId + " found.");
            } else {
                // copy all the retrieved data into persistent state
                Properties retrievedAccountData = allDataRetrieved.elementAt(0);
                persistentState = new Properties();

                Enumeration allKeys = retrievedAccountData.propertyNames();
                while (allKeys.hasMoreElements() == true) {
                    String nextKey = (String) allKeys.nextElement();
                    String nextValue = retrievedAccountData.getProperty(nextKey);
                    // patronId = Integer.parseInt(retrievedAccountData.getProperty("patronId"));

                    if (nextValue != null) {
                        persistentState.setProperty(nextKey, nextValue);
                    }
                }

            }
        }
        // If no account found for this user name, throw an exception
        else {
            throw new InvalidPrimaryKeyException("No account matching id : "
                    + patronId + " found.");
        }

    }
    //-----------------------------------------------------------------------------------
    public Patron(Properties props) {
        super(myTableName);

        setDependencies();
        persistentState = new Properties();
        Enumeration allKeys = props.propertyNames();
        while (allKeys.hasMoreElements() == true) {
            String nextKey = (String) allKeys.nextElement();
            String nextValue = props.getProperty(nextKey);

            if (nextValue != null) {
                persistentState.setProperty(nextKey, nextValue);
            }
        }
    }
    public Patron(){
        super(myTableName);
        setDependencies();
        persistentState = new Properties();
    }

    //-----------------------------------------------------------------------------------
    //myRegistry is coming from EntityBase
    private void setDependencies() {
        dependencies = new Properties();

        myRegistry.setDependencies(dependencies);
    }
    //-----------------------------------------------------------------------------------
    //persistentState represents one row of the database and consists of a primary key
    public Object getState(String key) {
        if (key.equals("patronId") == true)
            return persistentState.getProperty("patronId");
        if(key.equals("name") == true)
            return persistentState.getProperty("name");
        if(key.equals("address") == true)
            return persistentState.getProperty("address");
        if(key.equals("city") == true)
            return persistentState.getProperty("city");
        if(key.equals("stateCode") == true)
            return persistentState.getProperty("stateCode");
        if(key.equals("zip") == true)
            return persistentState.getProperty("zip");
        if(key.equals("email") == true)
            return persistentState.getProperty("email");
        if(key.equals("dateOfBirth") == true)
            return persistentState.getProperty("dateOfBirth");
        if(key.equals("status") == true)
            return persistentState.getProperty("status");


        return persistentState.getProperty(key);
    }
    //-----------------------------------------------------------------------------------

    public void stateChangeRequest(String key, Object value) {
        myRegistry.updateSubscribers(key, this);

    }

    //-----------------------------------------------------------------------------------
    protected void initializeSchema(String tableName) {
        if (mySchema == null) {
            mySchema = getSchemaInfo(tableName);
        }

    }

    //-----------------------------------------------------------------------------------
    public void save() {

        updateStateInDatabase();
    }

    //-----------------------------------------------------------------------------------
    private void updateStateInDatabase() {
        try {
            if (persistentState.getProperty("patronId") != null) {
                Properties whereClause = new Properties();
                whereClause.setProperty("patronId",
                        persistentState.getProperty("patronId"));
                updatePersistentState(mySchema, persistentState, whereClause);
                updateStatusMessage = "Patron information for PatronID: " + persistentState.getProperty("patronId") + " updated successfully in database!";
            } else {
                Integer patronId =
                        insertAutoIncrementalPersistentState(mySchema, persistentState);
                persistentState.setProperty("patronId", "" + patronId.intValue());
                updateStatusMessage = "Patron data for new Patron: " + persistentState.getProperty("patronId")
                        + " added successfully to database!";
            }
        } catch (SQLException ex) {
            updateStatusMessage = "Error in adding Patron data to database!";
        }
        System.out.println(updateStatusMessage + "\n");
    }
    //-----------------------------------------------------------------------------------
    public static int compare(Patron a, Patron b) {
        String aNum = (String) a.getState("patronId");
        String bNum = (String) b.getState("patronId");

        return aNum.compareTo(bNum);
    }
    //-----------------------------------------------------------------------------------
    public Vector<String> getEntryListView() {
        Vector<String> v = new Vector<String>();

        v.addElement(persistentState.getProperty("patronId"));
        v.addElement(persistentState.getProperty("name"));
        v.addElement(persistentState.getProperty("address"));
        v.addElement(persistentState.getProperty("city"));
        v.addElement(persistentState.getProperty("stateCode"));
        v.addElement(persistentState.getProperty("zip"));
        v.addElement(persistentState.getProperty("email"));
        v.addElement(persistentState.getProperty("dateOfBirth"));
        v.addElement(persistentState.getProperty("status"));

        return v;
    }
    //-----------------------------------------------------------------------------------
    public String toString(){
        return ("Patron Id: " + persistentState.getProperty("patronId") +
                " Name: " + persistentState.getProperty("name") +
                " Address: " + persistentState.getProperty("address") +
                " City: " + persistentState.getProperty("city") +
                " State: " + persistentState.getProperty("stateCode") +
                " Zip: " + persistentState.getProperty("zip") +
                " Email: " + persistentState.getProperty("email") +
                " Date Of Birth: " + persistentState.getProperty("dateOfBirth") +
                " Status: " + persistentState.getProperty("status") + '\n');
    }
    public void ProcessPatron(Properties p){

        persistentState.setProperty("name", p.getProperty("name"));
        persistentState.setProperty("address", p.getProperty("address"));
        persistentState.setProperty("city", p.getProperty("city"));
        persistentState.setProperty("stateCode", p.getProperty("stateCode"));
        persistentState.setProperty("zip", p.getProperty("zip"));
        persistentState.setProperty("email", p.getProperty("email"));
        persistentState.setProperty("dateOfBirth", p.getProperty("dateOfBirth"));
        persistentState.setProperty("status", p.getProperty("status"));

        save();


    }

}
