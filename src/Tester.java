/**
 * @author Becky Michael and Andy Duong
 * @version 2/6/2020
 * Tester for the Book, Patron, BookCollection and PatronCollection classes
 */

import exception.InvalidPrimaryKeyException;
import model.Book;
import model.BookCollection;
import model.Patron;
import model.PatronCollection;

import java.util.Properties;
import java.util.Scanner;
import java.util.Vector;

public class Tester {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        String cont = "Y";
        Scanner bookPat = new Scanner(System.in);

        System.out.println("Enter a Book");

        //----------------------------------------------------------------------------------------
        //Adding books into the system from user input
        //-----------------------------------------------------------------------------------------
        while ((cont.equals("Y") || cont.equals("y") || cont.equals("Yes") || cont.equals("yes")
                || cont.equals("YES"))) {
            myObj = new Scanner(System.in);
            System.out.println("Enter Book Title: ");
            String bookTitle = myObj.nextLine();
            System.out.println("Enter author: ");
            String author = myObj.nextLine();
            System.out.println("Enter pubYear ");
            String pubYear = myObj.next();
            System.out.println("Enter status: ");
            String status = myObj.next();
            //-----------------------------------------------------------------------------------
            Properties bookTest = new Properties();
            bookTest.setProperty("bookTitle", bookTitle);
            bookTest.setProperty("author", author);
            bookTest.setProperty("pubYear", pubYear);
            bookTest.setProperty("status", status);
            Book bookA = new Book(bookTest);
            bookA.save();
            System.out.println("This information has been successfully entered");
            System.out.println("Would you like to add another book? Enter Y for yes or N" +
                    " for no ");
            cont = myObj.next();
        }

        //--------------------------------------------------------------------------------------------
        // Insert a patron into the database
        //----------------------------------------------------------------------------------------------
        System.out.println("Enter a patron.");
        cont = "Y";
        while ((cont.equals("Y") || cont.equals("y") || cont.equals("Yes") || cont.equals("yes")
                || cont.equals("YES"))) {
            myObj = new Scanner(System.in);
            System.out.println("Enter Name: ");
            String name = myObj.nextLine();
            System.out.println("Enter address: ");
            String address = myObj.nextLine();
            System.out.println("Enter city: ");
            String city = myObj.next();
            System.out.println("Enter State: ");
            String stateCode = myObj.next();
            System.out.println("Enter Zip ");
            String zip = myObj.next();
            System.out.println("Enter Email: ");
            String email = myObj.next();
            System.out.println("Enter Date Of Birth: YYYY-MM-DD ");
            String dateOfBirth = myObj.next();
            System.out.println("Enter Status: ");
            String statusP = myObj.next();
            //-----------------------------------------------------------------------------------
            Properties patronTest = new Properties();
            patronTest.setProperty("name", name);
            patronTest.setProperty("address", address);
            patronTest.setProperty("city", city);
            patronTest.setProperty("stateCode", stateCode);
            patronTest.setProperty("zip", zip);
            patronTest.setProperty("email", email);
            patronTest.setProperty("dateOfBirth", dateOfBirth);
            patronTest.setProperty("status", statusP);

            Patron patronA = new Patron(patronTest);
            patronA.save();
            System.out.println("This information has been successfully entered");
            System.out.println("Would you like to add another Patron? Y for Yes N for No ");
            cont = myObj.next();

        }


        //----------------------------------------------------------------------------------------------
        myObj = new Scanner(System.in);
        System.out.println("Enter the book title to search for all books containing that title: ");
        String title = myObj.nextLine();
        BookCollection bookCol = new BookCollection();
        System.out.println("Collection of book titles containing " + title + ":");
        System.out.println(bookCol.findBooksWithTitleLike(title).toString());
        System.out.println();
        //-----------------------------------------------------------------------------------------------
        System.out.println("Enter a year to find books older than that year");
        String year = myObj.nextLine();
        System.out.println("Collection of books older than " + year + ":");
        System.out.println(bookCol.findBooksOlderThanDate(year).toString());
        System.out.println();
        //-------------------------------------------------------------------------------------------------
        System.out.println("Enter a year to find books newer than that year");
        String newYear = myObj.nextLine();
        System.out.println("Collection of books published after " + newYear + ":");
        System.out.println(bookCol.findBooksNewerThanDate(newYear).toString());
        System.out.println();
        //--------------------------------------------------------------------------------------------------
        PatronCollection patronCol = new PatronCollection();
        System.out.println("Enter a date of birth (YYYY-MM-DD) to find patrons younger than that date: ");
        String youYear = myObj.nextLine();
        System.out.println("Patrons born after " + youYear + ":");
        System.out.println(patronCol.findPatronsYoungerThan(youYear).toString());
        System.out.println();
        //--------------------------------------------------------------------------------------------------
        System.out.println("Enter a date of birth (YYYY-MM-DD) to find patrons older than date: ");
        String oldYear = myObj.nextLine();
        System.out.println("Patrons born before " + oldYear + ":");
        System.out.println(patronCol.findPatronsOlderThan(oldYear).toString());
        System.out.println();
        //--------------------------------------------------------------------------------------------------
        System.out.println("Enter a zipcode to find patrons with that zipcode");
        String zip = myObj.nextLine();
        System.out.println("Patrons with zipcode " + zip + " :");
        System.out.println(patronCol.findPatronsAtZipCode(zip).toString());
        //--------------------------------------------------------------------------------------------------


    }
}




